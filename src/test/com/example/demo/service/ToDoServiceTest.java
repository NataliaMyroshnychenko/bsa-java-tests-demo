package com.example.demo.service;

import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.repository.ToDoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ToDoServiceTest {

    private ToDoRepository toDoRepository;

    private ToDoService toDoService;

    //executes before each test defined below
    @BeforeEach
    void setUp() {
        this.toDoRepository = mock(ToDoRepository.class);
        toDoService = new ToDoService(toDoRepository);
    }

    @Test
    void whenInCompleteToDoIdNotFound_thenThrowsNotFoundException() {
        assertThrows(ToDoNotFoundException.class, () -> toDoService.completeToDo(1l));
    }

}
