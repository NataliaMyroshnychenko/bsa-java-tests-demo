package com.example.demo.controller;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Optional;

import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import com.example.demo.service.ToDoService;

@WebMvcTest(ToDoController.class)
@ActiveProfiles(profiles = "test")
@Import(ToDoService.class)
class ToDoControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ToDoRepository toDoRepository;

    @Test
    void whenSave_thenReturnCompletedValidResponse() throws Exception {
        ZonedDateTime completedAt = ZonedDateTime.now(ZoneOffset.UTC);
        ToDoEntity toDoEntity = new ToDoEntity(1l, "My to do text", completedAt);
        when(toDoRepository.findById(1l)).thenReturn(Optional.of(toDoEntity));

        this.mockMvc
                .perform(put("/todos/{id}/complete", 1l))
                .andDo(print())
                .andExpect(status().isOk());
        verify(toDoRepository, times(1)).findById(1l);
        verify(toDoRepository, times(1)).save(toDoEntity);
    }

    @Test
    void whenGetOneWithWrongId_thenReturnException() throws Exception {
        Long wrongId = 1l;
        when(toDoRepository.findById(anyLong())).thenReturn(Optional.empty());

        mockMvc.perform(get("/todos/{id}", wrongId))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(result -> assertNotNull(result.getResolvedException()))
                .andExpect(result -> assertEquals("Can not find todo with id " + wrongId, result.getResolvedException().getMessage()));
    }

    @Test
    void whenDelete_thenDelete() throws Exception {
        ToDoEntity toDoEntity = new ToDoEntity(11235l, "My to do text");
        when(toDoRepository.findById(1l)).thenReturn(Optional.of(toDoEntity));

        mockMvc.perform(delete("/todos/{id}", toDoEntity.getId()))
                .andExpect(status().isOk());
        verify(toDoRepository, times(1)).deleteById(toDoEntity.getId());
    }


}
