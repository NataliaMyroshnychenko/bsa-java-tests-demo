package com.example.demo.controller;

import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class ToDoControllerWithMockMVCIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ToDoRepository toDoRepository;

    @Test
    void whenGetOneWithValidId_thenReturnEntityResponse() throws Exception {

        ToDoEntity testEntity = createTestEntity();
        long id = testEntity.getId();

        mockMvc.perform(get("/todos/{id}", id))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(id))
                .andExpect(jsonPath("$.text").value("My to do text"));
    }

    private ToDoEntity createTestEntity() {
        ToDoEntity toDoEntity = new ToDoEntity(1l, "My to do text");
        return toDoRepository.save(toDoEntity);
    }

    @AfterEach
    public void resetDb() {

        toDoRepository.deleteById(1l);
    }

}
