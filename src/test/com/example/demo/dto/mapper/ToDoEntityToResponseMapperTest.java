package com.example.demo.dto.mapper;

import com.example.demo.dto.ToDoResponse;
import com.example.demo.model.ToDoEntity;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNull;


class ToDoEntityToResponseMapperTest {

    @Test
    void whenMapToDoEntity_thenReturnToDoResponse() {
        //mock
        ToDoEntity toDoEntity = new ToDoEntity(1l, "Test 1");
        toDoEntity.completeNow();
        //call
        ToDoEntityToResponseMapper.map(toDoEntity);
        //validate
        assertThat(toDoEntity)
                .hasFieldOrPropertyWithValue("id", toDoEntity.getId())
                .hasFieldOrPropertyWithValue("text", toDoEntity.getText())
                .hasFieldOrPropertyWithValue("completedAt", toDoEntity.getCompletedAt());
    }

    @Test
    void whenMapNull_thenReturnNull() {
        //mock
        ToDoEntity toDoEntity = null;
        //call
        ToDoResponse response = ToDoEntityToResponseMapper.map(toDoEntity);
        //validate
        assertNull(response);
    }
}

